<?php
require_once 'database.php';

if (isset($_SESSION['logado'])) {
	$usuario = $_SESSION['usuario']['nome'];
	$cod_user_filme = $_SESSION['usuario']['cod'];
	// echo "Bem vindo, " .$usuario;

	if((!isset($_POST['titulo'])) && (!isset($_POST['genero'])) && (!isset($_POST['data'])) && (!isset($_FILES['poster'])) && (!isset($_POST['desc']))  && (!isset($_FILES['url']))){
		header("Location: alterarFilmes.php");
		
	}else{

		$NewTitulo = $_POST['titulo'];
		$NewGenero = $_POST['genero'];
		$NewData = $_POST['data'];
		$NewPoster = $_FILES['poster'];
		$NewDesc = $_POST['desc'];
		$NewUrl = $_POST['url'];
		
	}	
}else{
	echo "Location: login.php";
}

if(!empty($NewPoster['name'])){

			//Verifica se o arquivo é uma imagem 
	if(!preg_match("/^image\/(jpeg|png|gif|bmp)$/", $NewPoster["type"])){ 

		echo "Isso não é uma imagem."; 

	}else{

	// Pega as dimensões do poster
		$dimensoes = getimagesize($NewPoster["tmp_name"]);   

		  // Pega extensão do poster
		preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $NewPoster["name"], $ext);   

		  // Pega o nome do poster
		$nome_poster = $NewPoster['name'];  

		   // Caminho de onde ficará a imagem 
		$caminho_imagem = "poster/" . $nome_poster;   

		   // Faz o upload da imagem para seu respectivo caminho 
		move_uploaded_file($NewPoster["tmp_name"], $caminho_imagem);
	}
}

$sql_alterar = "UPDATE filmes set titulo = ?, genero = ?, data = ?, poster = ?, descricao = ?, url = ? WHERE cod = ?";
$stmt = $con_db->prepare($sql_alterar);
	if($stmt->bind_param("ssssssi", $NewTitulo, $NewGenero, $NewData, $caminho_imagem, $NewDesc, $NewUrl, $_GET['cod'])){
		if($stmt->execute()){
			//$_SESSION['filmes'] = array('titulo' => $NewTitulo, 'genero' => $NewGenero, 'data' => $NewData, 'poster' => $caminho_imagem, 'desc' => $NewDesc, 'url' => $NewUrl);
			header("Location: listarFilmes.php");
		}
	}




?>