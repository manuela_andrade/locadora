	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container"> 
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> 
				</button>
				<a href="index.php" class="navbar-brand">Filmes Web</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Inicio</a></li>
					<li class="active"><a href="cadFilmesForm.php">Cadastrar Filmes</a></li>
					<li class="dropdown">
						<a href= "<script>$('.dropdown-toggle').dropdown()</script>" class="dropdown-toggle" data-toggle="dropdown">Meus Filmes
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="listarFilmes.php">Listar Filmes</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href= "<script>$('.dropdown-toggle').dropdown()</script>" class="dropdown-toggle" data-toggle="dropdown">Downloads
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="downloadDados.php?formato=xml">.XML</a></li>
							<li><a href="downloadDados.php?formato=csv">.CSV</a></li>
						</ul>
					</li>              
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-user"> </span> 
							<strong><?php echo $_SESSION['usuario']['nome']?></strong>
							<span class="glyphicon glyphicon-chevron-down"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="">
									<span class="glyphicon glyphicon-user "></span>
									<strong><?php echo $_SESSION['usuario']['nome']?></strong>
									<br />
									<?php echo $_SESSION['usuario']['email']?>
									</a>
							</li>

							<li class="divider"> </li>

							<li> 
								<a href="#">
									<span class="glyphicon glyphicon-pencil"> </span>
									Atualizar dados pessoais
								</a>
							</li>

							<li class="divider"> </li>

							<li>
								
								<a href="logout.php"> 
									<span class="glyphicon glyphicon-off"> </span>
									Logout
								</a>
							</li>




								
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<script src="bootstrap/jquery-1.10.2.js"></script>
	<script src="bootstrap/bootstrap.js"></script>
	

