<?php
require_once 'database.php';

	$return = "";
	$erro = false;
	$mensagem = array();
	$id = '';
	
	if (!isset($_POST['nome']) || empty($_POST['nome'])) {
		$erro = true;
		$mensagem[] = "O campo nome precisa ser preenchido";
	}
	
	if (!isset($_POST['email']) || empty($_POST['email'])) {
		$erro = true;
		$mensagem[] = "O campo email precisa ser preenchido";
	} else {
		$sql = "SELECT COUNT(*) AS total FROM usuarios WHERE email='{$_POST['email']}'";
		if($result = $con_db->query($sql)){
			$obj = $result->fetch_object();
			if($obj->total){
					$erro = true;
					$mensagem[] = "Email ja cadastrado";
			}
			$result->close();
		}
	}

	if (!isset($_POST['senha']) || empty($_POST['senha'])) {
		$erro = true;
		$mensagem[] = "O campo senha precisa ser preenchido";
	} else {
		$uppercase = preg_match('@[A-Z]@', $_POST['senha']);
		$lowercase = preg_match('@[a-z]@', $_POST['senha']);
		$number    = preg_match('@[0-9]@', $_POST['senha']);

		if (!$uppercase || !$lowercase || !$number || strlen($_POST['senha']) < 8) {
			$erro = true;
	 		$mensagem[] = "A senha precisar ter pelo menos 8 caracteres, letras maiúsculas e minúsculas e números";
		}
	}

	if (!$erro) {
		$nome = $_POST['nome'];
		$email = $_POST['email'];
		$senha = md5($_POST['senha']);

		if (isset($_FILES['foto']) && !empty($_FILES['foto']['name'])) {
			$imagem = $_FILES['foto'];
			if (!in_array($imagem['type'], array('image/jpeg', 'image/png')) || !@getimagesize($imagem["tmp_name"])) {
				$erro = true;
				$mensagem[] = "Imagem inválida";
			} else {
				 	$nome_imagem = md5_file($imagem["tmp_name"]) . '.jpg';

				   // Caminho de onde ficará a imagem 
					$caminho_imagem = "fotos/{$nome_imagem}";   

				   // Faz o upload da imagem para seu respectivo caminho 
					move_uploaded_file($imagem["tmp_name"], $caminho_imagem);  
			}
		} else {
			$nome_imagem = 'NoImageAvailable.gif';
		}

		$sql_cadastrar = "INSERT INTO usuarios VALUES(?,?,?,?,?)";
		$stmt = $con_db->prepare($sql_cadastrar);
		if($stmt->bind_param("issss", $id, $email, $nome, $senha, $nome_imagem)){
			$return = $stmt->execute() ? 1 : 0;
		}

		$stmt->close();
		$con_db->close();

		if($erro==false) $mensagem[] = "Cadastro realizado com sucesso";
	}

	echo json_encode(array("erro"=>$erro,"mensagem"=>$mensagem));
?>