	<?php
	
	session_start();

	if (!isset($_SESSION['logado'])) {
		header("Location: login.html");
	}

	
	header('Content-Type: text/html; charset=utf-8');

	?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/index.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/cadastro_filme.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	</head>

	<body>	
	<?php include "includes/navbar.php"; ?>

		<section class="container">
			<div class="row">
				<div class="col-md-12"> 
				 
				
				<h1>Cadastrar Filmes</h1>

				<div id="cadFilmes-alert" style="display:none;"></div>


					<!-- Formulário de cadastro de filmes-->
					<form id="cadFilmeForm" role="form" class="well" action="cadastrarFilme.php" method="post" enctype="multipart/form-data" >
						
						<!-- Titulo -->
						<div class="form-group">
							<label for="titulo">Título</label>
							<input type="text" name="titulo" class="form-control" placeholder="Enter title" onkeyup="verificar('nome')" >
							<label id="ico_nome"> </label>


						</div>

						<!-- Genero -->
						<div class="form-group">

							<label for="InputGenero">Gênero</label>
							<select name="genero" >
								<?php

								$generos = array('Ação', 'Animação', 'Aventura', 'Chanchada', 'Cinema catástrofe', 'Comédia', 'Comédia romântica', 'Comédia dramática', 
									'Comédia de ação', 'Cult', 'Dança', 'Documentários', 'Drama', 'Espionagem', 'Erótico', 'Fantasia', 'Faroeste', 'Ficção científica', 'Franchise/Séries', 
									'Guerra', 'Machinima', 'Masala', 'Musical', 'Filme noir', 'Policial', 'Pornográfico', 'Romance', 'Suspense', 'Terror', 'Trash');

								echo "<option value=' '> Selecionar Gênero </option>";
								foreach ($generos as $genero) {
									$valor = $genero;
									echo "<option value=\"$valor\"> $valor </option>";
								}
								?>
							</select>

						</div>


						<!-- Data de lancamento -->
						<div class="form-group">
							<label for="data">Data de lançamento</label>
							<input type="date" id="data" name="data" class="form-control" >
							
						</div>

						<!-- Descricao -->

						<div class="form-group">
							<label for="desc">Sinopse</label>
							<textarea name="desc" class="form-control" rows="4" cols="50"></textarea>
						</div>

						<!-- URL -->
						<div class="form-group">
							<label for="url">Trailer</label>
							<input type="text" id="url" name="url" class="form-control" placeholder="Enter url"> 
							<label id="ico_nome"> </label>


						</div>

						<!-- Poster -->
						<div class="form-group">
							<label for="exampleInputFile">Poster</label>
							<input type="file" name="poster" id="poster">
							<p class="help-block">Tipos permitidos: JPG, PNG.</p>
						</div>

						<button type="submit" class="btn btn-default">Cadastrar Filme</button>
						
					</form>

					<div id="cadFilmesXML-alert" style="display:none;"></div>
					<form id="cadastrar_xmlForm" method="post" action="cadastra_filme_xml.php" class="well" enctype="multipart/form-data">
						<label>Cadastrar com arquivo XML</label>
						<input type="file" name="cadastrar_xml" id='xml'>
						<button type = "submit" class="btn btn-default">Cadastrar via XML</button>
					</form>

				</div>
			</div>	

			
 
			<!-- </div> -->

		</section>

		<script src="scripts/jquery-2.1.1.min.js"></script>
		<script>
			$("#cadFilmeForm").on("submit", function(e) {
				e.preventDefault();

				$.ajax({
					url: 'cadastrarFilme.php',
					type: 'POST',
					data: new FormData(this),
					processData: false,
      				contentType: false,
      				dataType: 'json'
				}).done(function(data) {
					if(!data.erro) {
						$('#cadFilmes-alert').html('<div class="alert alert-success" role="alert">' + data.mensagem.join("<br/>") + '</div>').fadeIn(1000).delay(3000).fadeOut(1000);
						// window.location="listarFilmes.php";
					} else {
						$('#cadFilmes-alert').html('<div class="alert alert-danger" role="alert">' + data.mensagem.join("<br/>") + '</div>').fadeIn(1000).delay(3000).fadeOut(1000);
					}
				});
				return false;
			});

			$("#cadastrar_xmlForm").on("submit", function(e) {
				e.preventDefault();

				$.ajax({
					url: 'cadastra_filme_xml.php',
					type: 'POST',
					data: new FormData(this),
					processData: false,
      				contentType: false,
      				dataType: 'json'
				}).done(function(data) {
					if(!data.erro) {
						// $('#cadFilmesXML-alert').html('<div class="alert alert-success" role="alert">' + data.mensagem.join("<br/>") + '</div>').fadeIn(1000).delay(3000).fadeOut(1000);
						 window.location="listarFilmes.php";
					} else {
						$('#cadFilmesXML-alert').html('<div class="alert alert-danger" role="alert">' + data.mensagem.join("<br/>") + '</div>').fadeIn(1000).delay(3000).fadeOut(1000);
					}
				});
				return false;
			});
			</script>

	</body>
	</html>