<!DOCTYPE html>
<html>
<head>

	<title>Meus Filmes</title>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/listarfilmes.css">
	<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/simple-sidebar.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/navbar.css"> -->
	<!-- <script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-dropdown.js"></script> -->
	<!-- <script src="http://code.jquery.com/jquery-latest.js"></script> -->
</head>
<body>

	<header>
		
	</header>


	<?php

	session_start();

	if (isset($_SESSION['logado'])) {
		$usuario = $_SESSION['usuario']['nome'];
		$cod_user_filme = $_SESSION['usuario']['cod'];
	}else{
		header("Location: login.html");
	}

	include "includes/navbar.php";


		//resgata o arquivo (xml) de configuracao do banco de dados
	$config_db = simplexml_load_file("xml/conexao.xml");

		// Conecta com o banco atraves de um arquivo xml
	$con_db = new mysqli($config_db->host, $config_db->usuario, $config_db->senha, $config_db->banco);

	if(!$con_db){
		echo "Nao foi possivel conectar com o banco de dados" .mysqli_connect_errno();
		exit;
	}

	$sql_listarFilmes = "SELECT * FROM filmes";

	if($result = $con_db->query($sql_listarFilmes)): ?>

	<div class="container">

		<?php while ($obj = $result->fetch_object()): ?>

			<div class="movie_container row col-md-4">
				<div class="col-md-12">
					<ul class="thumbnails">
						<li class='col-md-4'>
							<div class="filme_">
								<a class="thumbnail thumb-container" href="#">
									<img class="filme" src='<?php echo $obj->poster; ?>'></img>
								</a> 
									<!-- <form action='alterarFilme.php' method='POST'>

                                        <input type='hidden' name='alterar' value='<?php echo $obj->cod; ?>'/>
                                        <input type='submit' class='btn btn-default' value="Alterar"/>
                                    </form> -->
                                </div>
                                <!-- End .filme_ -->
                            </li>

                            <!-- <section class="informacoes"> -->
                            <li class="col-md-6">
                            	<ul class='movie_info_content'>
                            		<li>
                            			Código: <?php echo $obj->cod; ?> .
                            		</li>

                            		<li>
                            			Título: <?php echo $obj->titulo; ?> .
                            		</li>

                            		<li>
                            			Gênero: <?php echo $obj->genero; ?> .
                            		</li>

                            		<li>
                            			Data de Lançamento: <?php echo date("d/m/Y",strtotime($obj->data)); ?> .
                            		</li>

                            		<li>
                            			Descrição: <?php echo $obj->descricao ?> .
                            		</li>

                            		<li>
                            			Video: 
                            			<a href="<?php echo $obj->url; ?>" target="_blank"><span class="glyphicon glyphicon-facetime-video"></span></a>
                            		</li>
                            	</ul>
                            	<!-- End .movie_info_content -->
                            </li>

                        </ul>
                        <!-- End thumbnails -->
                    </div>
                </div>

        <?php endwhile; ?>

        </div>
        <!-- End .movie_container -->


        <?php $result->close(); ?>

    </div>
    <!-- End .container -->

<?php endif; ?>

<?php $con_db->close(); ?>

</body>
</html>



