<?php
require_once 'database.php';

function downloadDadosCSV(){
    global $con_db;

	$cod_user_filme = $_SESSION['usuario']['cod'];

	// create a file pointer connected to the output stream
	$output = fopen('dadosFilmes.csv', 'w');

	// output the column headings
	fputcsv($output, array('Titulo', 'Genero', 'Data', 'Poster', 'Sinopse', 'URL'));

	$sql = "SELECT titulo, genero, data,poster, descricao, url FROM filmes WHERE cod_user_filme = ?";
	$stmt = $con_db->prepare($sql);

	if($stmt->bind_param("i", $cod_user_filme)){
		$stmt->execute();
		if ($stmt->bind_result($titulo, $genero, $data, $caminho_imagem, $desc, $url)) {
			while ($stmt->fetch()) {
				//var_dump($linhas);
				
				fputcsv($output, array($titulo, $genero, $data, $caminho_imagem, $desc, $url));
			}
		}
	}

	fclose($output);

	header('Content-Disposition: attachment; filename=dadosFilmes.csv');
	header("Content-Length: " . filesize('dadosFilmes.csv'));
	readfile('dadosFilmes.csv');
	exit;
}

function downloadDadosXML($cod_user_filme){

	global $con_db;

	$sql = "SELECT titulo, genero, data,poster, descricao, url FROM filmes WHERE cod_user_filme = ?";
	$stmt = $con_db->prepare($sql);

	$dom = new DOMDocument('1.0', 'UTF-8');
	$dom->preserveWhiteSpaces = false;
	$dom->formatOutput = true;

	$nodeFilmes = $dom->createElement('filmes');

	if($stmt->bind_param("i", $cod_user_filme)){
		$stmt->execute();
		if ($stmt->bind_result($titulo, $genero, $data, $caminho_imagem, $desc, $url)) {
			while ($stmt->fetch()) {

				$nodeFilme = $dom->createElement('filme');
				$nodeTitulo = $dom->createElement('titulo', $titulo);
				$nodeGenero = $dom->createElement('genero', $genero);
				$nodeData = $dom->createElement('data', $data);
				$nodePoster = $dom->createElement('poster', $caminho_imagem);
				$nodeDesc = $dom->createElement('sinopse', $desc);
				$nodeURL = $dom->createElement('url', $url);

				$nodeFilme->appendChild($nodeTitulo);
				$nodeFilme->appendChild($nodeGenero);
				$nodeFilme->appendChild($nodeData);
				$nodeFilme->appendChild($nodePoster);
				$nodeFilme->appendChild($nodeDesc);
				$nodeFilme->appendChild($nodeURL);

				$nodeFilmes->appendChild($nodeFilme);
			}
		}
	}

	$dom->appendChild($nodeFilmes);
	
		header("Content-Type: text/xml; charset=utf-8",true); //Para exibir no proprio navegador o arquivo xml
    	header("Content-Disposition: attachment; filename = 'dadosFilmes.xml'; charset=utf-8",true);//Para baixar o arquivo - força download

		// header('Content-type: text/xml');
		// header('Content-Disposition: attachment; filename="dadosFilmes.xml"');
		echo $dom->saveXML();

	exit;
}

if (isset($_SESSION['logado']) && $_SESSION['logado']){

	$user_id = $_SESSION['usuario']['cod'];
	if($_GET['formato'] == 'xml'){
		downloadDadosXML($user_id);
	}else{
		downloadDadosCSV($user_id);
	}
}

?>