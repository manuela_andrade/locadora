<nav class="navbar navbar-default navbar-fixed-top" role="banner">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="/" class="navbar-brand">Filmes Web</a>
		</div>
		<nav class="collapse navbar-collapse" role="navigation">
			<ul class="nav navbar-nav">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="cadFilmesForm.php">Cadastrar Filmes</a>
				</li>
				<li>
					<a href="listarFilmes.php">Listar Filmes</a>
				</li>
				<li>
					<a href="logout.php">Logout</a>
				</li>
				</ul>
			</nav>
		</div>
	</nav>