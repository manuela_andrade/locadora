<?php
	require_once 'database.php';

	if(!isset($_SESSION['logado'])) {
		header("Location: login.html");
	}

	$erro = false;

	// Query to remove
	$sql_remover = "DELETE FROM filmes WHERE cod=?";

	// Prepare query
	$stmt = $con_db->prepare($sql_remover);

	// Bind values
	$stmt->bind_param('i', $_GET['cod']);

	if ($stmt->execute()) {
		header("Location: listarFilmes.php");
	}
	else {
		$erro = true;
		$mensagem[]="Não foi possível remover o filme.";
	}



?>