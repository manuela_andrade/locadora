<?php
	
	session_start();

	if (!isset($_SESSION['logado'])) {
		header("Location: login.html");
	}
	header('Content-Type: text/html; charset=utf-8');
	//include "includes/navbar.php";

	//resgata o arquivo (xml) de configuracao do banco de dados
	$config_db = simplexml_load_file("xml/conexao.xml");

		// Conecta com o banco atraves de um arquivo xml
	$con_db = new mysqli($config_db->host, $config_db->usuario, $config_db->senha, $config_db->banco);

	if(!$con_db){
		echo "Nao foi possivel conectar com o banco de dados" .mysqli_connect_errno();
		exit;
	}

	if(isset($_GET['cod'])){
		$cod_filme = $_GET['cod'];
	}


	$sql = "SELECT titulo, genero, data,poster, descricao, url FROM filmes WHERE cod = ?";
	$stmt = $con_db->prepare($sql);

	if($stmt->bind_param("i", $cod_filme)){
		if($stmt->execute()){
			if($stmt->bind_result($titulo, $genero, $data, $caminho_imagem, $desc, $url)){
				while ($stmt->fetch()) {

					$filme_info = array('cod' => $cod_filme, 'titulo' => $titulo, 'genero' => $genero, 'data' => $data, 'poster' => $caminho_imagem, 'desc' => $desc, 'url' =>$url );

				}
			}
		}
	}


	?>

<html>
	<head>
		<meta charset="UTF-8">
		<!-- <meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> -->
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/index.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/cadastro_filme.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	</head>
	<?php include "includes/navbar.php"; ?>

	<body>	
		<section class="container">
			<div class="row">
				<div class="col-md-12"> 
				<h1>Alterar Filme</h1>
		
					<!-- Formulário de cadastro de filmes-->
					<form role="form" class="well" action="alt_Filme.php?cod=<?php echo $_GET['cod']; ?>" method="post" enctype="multipart/form-data" >
						
						<!-- Titulo -->

						<div class="form-group">
							<label for="titulo">Título</label>
							<input type="text" name="titulo" class="form-control" value="<?php echo $filme_info['titulo']?>" >
							<label id="ico_nome"> </label>


						</div>

						<!-- Genero -->
						<div class="form-group">

							<label for="InputGenero">Gênero</label>
							<select name="genero">

							<?php   $generos = array('Ação', 'Animação', 'Aventura', 'Chanchada', 'Cinema catástrofe', 'Comédia', 'Comédia romântica', 'Comédia dramática', 
									'Comédia de ação', 'Cult', 'Dança', 'Documentários', 'Drama', 'Espionagem', 'Erótico', 'Fantasia', 'Faroeste', 'Ficção científica', 'Franchise/Séries', 
									'Guerra', 'Machinima', 'Masala', 'Musical', 'Filme noir', 'Policial', 'Pornográfico', 'Romance', 'Suspense', 'Terror', 'Trash');

								echo "<option value=' '> Selecionar Gênero </option>";
								foreach ($generos as $genero) {
									$valor = $genero;
									?>
								<option value="<?php echo $valor ?> " <?php if($valor == $filme_info['genero']):?> selected <?php endif?> > <?php echo $valor ?> </option>";
							<?php } ?>
						
							</select>

						</div>


						<!-- Data de lancamento -->
						<div class="form-group">
							<label for="data">Data de lançamento</label>
							<input type="date" id="data" name="data" class="form-control" value="<?php echo $filme_info['data']?>">
							
						</div>

						<!-- Descricao -->

						<div class="form-group">
							<label for="desc">Sinopse</label>
							<textarea name="desc" class="form-control" rows="4" cols="50"> <?php echo $filme_info['desc']?> </textarea>
						</div>

						<!-- URL -->
						<div class="form-group">
							<label for="url">Trailer</label>
							<input type="text" id="url" name="url" class="form-control" value="<?php echo $filme_info['url']?>"> 
							<label id="ico_nome"> </label>


						</div>

						<!-- Poster -->
							<div class='thumbnails col-md-12'>
							<a class="thumbnail img-responsive col-md-3" href="#"><img class=\"filme\" src='<?php echo $filme_info['poster']?>'></img></a>
							</div>
							<div class="form-group">
							<label for="exampleInputFile">Poster</label>
								<input type="file" name="poster" id="poster">
								<p class="help-block">Tipos permitidos: JPG, PNG.</p>
						</div>

						<button type="submit" class="btn btn-default">Alterar Filme</button>
					</form>

				</div>
			</div>	

			

			<!-- </div> -->

		</section>		

	</body>
	</html>
