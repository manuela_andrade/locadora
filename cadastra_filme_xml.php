<?php 
require_once 'database.php';

if (isset($_SESSION['logado'])) {
	$cod_user_filme = $_SESSION['usuario']['cod'];
	// echo "Bem vindo, " .$usuario;
}else{
	header("Location: login.php"); 

}

$erro = false;
$mensagem= array();


if (isset($_FILES['cadastrar_xml']['tmp_name']) && !empty($_FILES['cadastrar_xml']['tmp_name'])) {
	//echo"entrei no if";

	$xmlFileName = $_FILES['cadastrar_xml']['name'];

	if (move_uploaded_file($_FILES['cadastrar_xml']['tmp_name'], ('xml/' . $xmlFileName))) {
		if(file_exists('xml/' . $xmlFileName)) {
			$xml = simplexml_load_file('xml/' . $xmlFileName);

			$id = '';
			$titulo = (string)$xml->titulo;
			$genero = (string)$xml->genero;
			$data_lancamento = (string)$xml->data;
			$poster = 'poster/default.jpg';
			$descricao = (string)$xml->descricao;
			$url = (string)$xml->url;

		}
	}


				// Query to insert
	$query = "INSERT INTO filmes VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

				// Prepare query
	$stmt = $con_db->prepare($query);

				// Bind parameters
	$stmt->bind_param('ssssssss', $id, $titulo, $genero, $data_lancamento, $poster, $descricao, $url, $cod_user_filme);

				// Execute
	if ($stmt->execute()) {
		$mensagem[] = "Cadastro realizado com sucesso!";
	}


	$stmt->close();


}else{

	$erro = true;
	$mensagem[] = "Nenhum arquivo selecionado";
}
$con_db->close();
die(json_encode(array("erro"=>$erro,"mensagem"=>$mensagem)));

?>

