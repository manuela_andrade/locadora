function ajaxEmail(str)
		{
		if (str.length==0)
		  { 
		  document.getElementById("ajax_email").innerHTML="";
		  document.getElementById("ajax_email").style.border="0px";
		  return;
		  }
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("ajax_email").innerHTML=xmlhttp.responseText;
		    //document.getElementById("ajax_email").style.border="1px solid #A5ACB2";
		    }
		  }
		xmlhttp.open("GET","ajax_email.php?q="+str,true);
		xmlhttp.send();
		}

		
function verificar (campo) {
	switch(campo){
			case "nome" : 
			var ico = document.getElementById("ico_nome");
			if (!/^[A-z ]+$/.test(document.getElementById("nome").value)) {
				ico.innerHTML = "<i class='fa fa-times-circle-o'></i>";
			}
			else{
				ico.innerHTML = "<i class='fa fa-check-circle-o'></i>";
			}
			if (document.getElementById("nome").value=="") {
				ico.innerHTML = "";
			}
			break;

		case "email" : 
			var ico = document.getElementById("ico_email");
			if (!/^(\w|[._%-])+\@(\w|[.-])+\.[a-z]{2,4}$/.test(document.getElementById("email").value)) {
				ico.innerHTML = "<i class='fa fa-times-circle-o'></i>";
			}else{
				ico.innerHTML = "<i class='fa fa-check-circle-o'></i>";	
			}
			if (document.getElementById("email").value == "") {
				ico.innerHTML = "";
			}
			break;

		case "senha" : 
			var ico = document.getElementById("ico_senha");
			if (!/^(?=.*[0-9])(?=.*[A-z])[a-zA-Z0-9!@#$%^&*]{5,10}$/.test(document.getElementById("senha").value)) {
				ico.innerHTML = "<i class='fa fa-times-circle-o'></i>";
			}
			else{
				ico.innerHTML = "<i class='fa fa-check-circle-o'></i>";
			}
			if (document.getElementById("senha").value=="") {
				ico.innerHTML = "";
			}
			break;

		case "confsenha" : 
			var ico = document.getElementById("ico_confsenha");
			if (!/^(?=.*[0-9])(?=.*[A-z])[a-zA-Z0-9!@#$%^&*]{5,10}$/.test(document.getElementById("confsenha").value)) {
				ico.innerHTML = "<i class='fa fa-times-circle-o'></i>";
			}
			else{
				ico.innerHTML = "<i class='fa fa-check-circle-o'></i>";
			}
			if (document.getElementById("confsenha").value=="") {
				ico.innerHTML = "";
			}
			break;

			case "oldsenha" : 
			var ico = document.getElementById("ico_oldsenha");
			if (!/^(?=.*[0-9])(?=.*[A-z])[a-zA-Z0-9!@#$%^&*]{5,10}$/.test(document.getElementById("oldsenha").value)) {
				ico.innerHTML = "<i class='fa fa-times-circle-o'></i>";
			}
			else{
				ico.innerHTML = "<i class='fa fa-check-circle-o'></i>";
			}
			if (document.getElementById("oldsenha").value=="") {
				ico.innerHTML = "";
			}
			break;

		
	}
}
