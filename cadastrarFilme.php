<?php
require_once 'database.php';

if (isset($_SESSION['logado'])) {
	$usuario = $_SESSION['usuario']['nome'];
	$cod_user_filme = $_SESSION['usuario']['cod'];
	// echo "Bem vindo, " .$usuario;
}else{
	header("Location: login.html");
}

$erro = false;
$mensagem = array();


	$regexTitulo ="/^([a-z A-Z]|\d)+(-?(\w)?|\d)*/";
	$regexData = "/[0-9]\d{3}\-[0-9]\d{1}\-[0-9]\d{1}/";

	if(!isset($_POST['titulo']) || empty($_POST['titulo'])) {
		$erro = true;
		$mensagem[] = "Por favor, preencher o campo título";
	}else if(!preg_match($regexTitulo, $_POST['titulo'])) {
		$erro = true;
		$mensagem[] = "Título inválido";
	}

	if(!isset($_POST['genero']) || empty($_POST['genero'])){
		$erro = true;
		$mensagem[] = "Por favor, preencher o campo gênero";

	} 
	if(!isset($_POST['data']) || empty($_POST['data'])){
		$erro = true;
		$mensagem[] = "Por favor, preencher o campo data";

	} else if(!preg_match($regexData, $_POST['data'])) {
		$erro = true;
		$mensagem´[] = "Formato de data inválido";
	}

	if(!isset($_POST['desc']) || empty($_POST['desc'])){
		$erro = true;
		$mensagem[] = "Por favor, preencher o campo sinopse";
	}  
	if(!isset($_POST['url']) || empty($_POST['url'])){
		$erro = true;
		$mensagem[] = "Por favor, preencher o campo URL";
	}

	$cod = '';
	$titulo = $_POST['titulo'];
	$genero = $_POST['genero'];
	$data = $_POST['data'];
	$poster = $_FILES['poster'];
	$desc = $_POST['desc'];
	$url = $_POST['url'];


$defaultImage = "poster/default.jpg";

$sql_cadastrarFilme = "INSERT INTO filmes VALUES(?,?,?,?,?,?,?,?)";
$stmt = $con_db->prepare($sql_cadastrarFilme);

//se o poster tiver sido selecionado
if(!empty($poster['name'])){

			//Verifica se o arquivo é uma imagem 
	if(!preg_match("/^image\/(jpeg|png|gif|bmp)$/", $poster["type"])){ 

		echo "Isso não é uma imagem."; 

	}else{

	// Pega as dimensões do poster
		$dimensoes = getimagesize($poster["tmp_name"]);   

		  // Pega extensão do poster
		preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $poster["name"], $ext);   

		  // Pega o nome do poster
		$nome_poster = $poster['name'];  

		   // Caminho de onde ficará a imagem 
		$caminho_imagem = "poster/" . $nome_poster;   

		   // Faz o upload da imagem para seu respectivo caminho 
		move_uploaded_file($poster["tmp_name"], $caminho_imagem);   

	}
}else{
	$caminho_imagem = 'poster/default.jpg';
}

   // Insere os dados no banco atraves do bind param
		if($stmt->bind_param("issssssi", $cod, $titulo, $genero, $data, $caminho_imagem, $desc, $url, $cod_user_filme)){
			if(!$stmt->execute()){
				$erro = true;
				$mensagem[] = "Erro ao cadastrar filme";
			}
		}

if($erro==false) $mensagem[] = "Cadastro efetuado.";

die(json_encode(array("erro"=>$erro,"mensagem"=>$mensagem)));

$stmt->close();
$con_db->close();





?>