<?php require_once 'database.php'; ?>
<!DOCTYPE html>
<html>
<head>

	<title>Meus Filmes</title>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/listarfilmes.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/simple-sidebar.css">
	<link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
</head>
<body>

	<header>
		<h2 class="col-md-12 text-center">Meus filmes</h2>
	</header>


	<?php


	if (isset($_SESSION['logado'])) {
		$usuario = $_SESSION['usuario']['nome'];
		$cod_user_filme = $_SESSION['usuario']['cod'];
	}else{
		header("Location: login.html");
	}

	include "includes/navbar.php";


	$sql_listarFilmes = "SELECT * FROM filmes WHERE cod_user_filme = ?";
	$stmt = $con_db->prepare($sql_listarFilmes);


	if($stmt->bind_param("i", $cod_user_filme)):
		$stmt->execute(); 
		if($stmt->bind_result($cod, $titulo, $genero, $data, $caminho_imagem, $desc, $url, $cod_user_filme)): ?>

			<div class="container col-md-12">

			<?php while ($linhas = $stmt->fetch()): ?>

				<div class="movie_container row col-md-4">
					<div class="col-md-12">
						<ul class="thumbnails">
							<li class='col-md-4'>
							<form id='removerFilmeForm'>
								<div class="filme_">
									<a class="thumbnail thumb-container" href="#">
										<img class="filme" src='<?php echo $caminho_imagem; ?>'></img>
									</a> 
                                    <a href="alterarFilme.php?cod=<?php echo $cod; ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                    <a href="removerFilme.php?cod=<?php echo $cod; ?>"><span class="glyphicon glyphicon-trash" onclick="return confirm('Deseja remover filme?')"></span></a>									
								</div>
								</form>
								<div id="removerFilme-alert" style="display:none;"></div>
								
								<!-- End .filme_ -->
							</li>
					
							<!-- <section class="informacoes"> -->
							<li class="col-md-6">
								<ul class='movie_info_content'>
									<li>
										Código: <?php echo $cod; ?> .
									</li>

									<li>
										Título: <?php echo $titulo; ?> .
									</li>

									<li>
										Gênero: <?php echo $genero; ?> .
									</li>

									<li>
										Data de Lançamento: <?php echo date("d/m/Y",strtotime($data)); ?> .
									</li>

									<li>
										Descrição: <?php echo $desc ?> .
									</li>

									<li>
										Video: 
										<a href="<?php echo $url; ?>" target="_blank"><span class="glyphicon glyphicon-facetime-video"></span></a>
									</li>
								</ul>
								<!-- End .movie_info_content -->
							</li>

						</ul>
						<!-- End thumbnails -->
					</div>
				</div>
			<!-- 	
				</div> -->
				<!-- End .movie_container -->
			<?php endwhile; ?>


			<?php $stmt->close(); ?>

		</div>
		<!-- End .container -->

	<?php endif; ?>
	<?php endif; ?>

			<?php $con_db->close(); ?>

		</body>
	</html>