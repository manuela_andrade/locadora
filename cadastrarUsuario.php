<?php

session_start();

if((!isset($_POST['nome'])) && (!isset($_POST['email'])) && (!isset($_POST['senha'])) && (!isset($_POST['confirma_senha']))){
	echo "Por favor preencher todos os campos";
	header("Location: login.html");
} else{
		// echo "Usuario cadastrado com sucesso";
	$cod = '';
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$senha = md5($_POST['senha']);
	$foto = $_FILES["foto"];

		//resgata o arquivo (xml) de configuracao do banco de dados
	$config_db = simplexml_load_file("xml/conexao.xml");

		// Conecta com o banco atraves de um arquivo xml
	$con_db = new mysqli($config_db->host, $config_db->usuario, $config_db->senha, $config_db->banco);

	if(!$con_db){
		echo "Nao foi possivel conectar com o banco de dados" .mysqli_connect_errno();
		exit;
	}


	$sql_cadastrar = "INSERT INTO usuarios VALUES(?,?,?,?,?)";
	$stmt = $con_db->prepare($sql_cadastrar);
	//var_dump($stmt);

		// Se a foto estiver sido selecionada
	if (!empty($foto["name"])) {   
		var_dump($_FILES);

			//Verifica se o arquivo é uma imagem 
		if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto["type"]) ){ 
			
			echo "Isso não é uma imagem."; 

		}else{

			// Pega as dimensões da imagem 
			$dimensoes = getimagesize($foto["tmp_name"]);   

		  // Pega extensão da imagem 
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);   

		  // Pega o nome da imagem 
			$nome_imagem = $foto['name'];  

		   // Caminho de onde ficará a imagem 
			$caminho_imagem = "fotos/" . $nome_imagem;   

		   // Faz o upload da imagem para seu respectivo caminho 
			move_uploaded_file($foto["tmp_name"], $caminho_imagem);   

		   // Insere os dados no banco atraves do bind param
			if($stmt->bind_param("issss", $cod, $email, $nome, $senha, $nome_imagem)){
				if($stmt->execute()){
		   		// Se os dados forem inseridos com sucesso 
					echo "<div class=\"alert alert-success\" role=\"alert\">Cadastrado com sucesso!</div>";
				}else{
					echo "<div class=\"alert alert-danger\" role=\"alert\">Erro ao cadastrar!</div>";
				}

			}
		}

		header("Location: login.html");
	} 

	$stmt->close();
	$con_db->close();
}




?>